const axios = require('axios');

axios.get("http://senacao.tk/objetos/computador_array_objeto")
.then(function(res){
   const computador = res.data;
    console.log(computador.marca);
    console.log(computador.modelo);
    console.log(computador.memoria);
    console.log(computador.ssd);
    console.log(computador.processador);

    console.log(computador.softwares[2]);

    computador.softwares.forEach(function(softwares, index){
        console.log(`softwares ${index+1}: ${softwares}`);
    });

    console.log(computador.fornecedor.rsocial);
    console.log(computador.fornecedor.telefone);
    
})